const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const validator = require('validator');

/* ---USERS--- */

// creating a schema for user
const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        validate(value) {
            if (!validator.isEmail(value))
                throw new Error('Enter valid email');
        }
    },
    password: {
        type: String,
        required: true,
        minLength: 6,
        validate(value) {
            if (value.toLowerCase().includes('password'))
                throw new Error('Password cannot include "password"');
        }
    },
    age: {
        type: Number,
        required: true,
        validate(value) {
            if (value <= 0)
                throw new Error('Age must be a positive number.');
        }
    },
    tokens: [{
        token: {
            type: String,
            required: true,
        }
    }],
});

userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({ email: email });
    if(!user)
        throw new Error('Unable to login');

    const isMatch = await bcrypt.compare(password, user.password);
    if(!isMatch)
        throw new Error('Unable to login');

    return user;
};

userSchema.methods.generateAuthToken = async function () {
    const user = this;

    const token = jwt.sign({ _id: user._id.toString() }, 'VishalTank');
    user.tokens = user.tokens.concat({ token: token });
    await user.save();

    return token;
};

userSchema.pre('save', async function(next) {
    const user = this;

    if(user.isModified('password'))
        user.password = await bcrypt.hash(user.password, 8);

    next();
});

// create a model for user
const User = mongoose.model('User', userSchema);


module.exports = User;
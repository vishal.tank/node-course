const mongoose = require('mongoose');
const validator = require('validator');

/* ---TASKS--- */

// create a shema for tasks
const taskSchema = new mongoose.Schema({
    description: {
        type: String,
        required: true,
        trim: true,
    },
    completed: {
        type: Boolean,
        default: false,
    },
});

// create a model for tasks
const TaskModel = mongoose.model('Task', taskSchema);

// // create instance of the model
// const task = new TaskModel({ description: 'Description', completed: false });

// //save the data to db
// task.save().then((task) => {
//     console.log(task);
// }).catch((error) => {
//     console.log(error.message);
// });


module.exports = TaskModel;
const express = require('express');
const Task = require('../models/tasksModel');
const router = new express.Router();

/* ---TASKS--- */

router.get('/tasks', (req, res) => {

    Task.find({})
        .then((users) => {
            res.send(users);
        }).catch((error) => {
            res.status(400).send(error.message);
        });
});

router.get('/tasks/:id', (req, res) => {
    Task.findById(req.params.id).then((task) => {
        if (!task)
            return res.status(404).send('Task NOT found');

        res.status(200).send(task);
    }).catch((error) => {
        res.status(400).send(error.message);
    });
});

router.post('/tasks', (req, res) => {
    const task = new Task(req.body);

    task.save().then(() => {
        res.status(201).send(task);
    }).catch((error) => {
        res.status(400).send(error.message);
    });
});

router.patch('/tasks/:id', async (req, res) => {

    const updates = Object.keys(req.body);
    const allowedUpdates = ['description', 'completed'];

    const isValidUpdate = updates.every((update) => allowedUpdates.includes(update));

    if (!isValidUpdate)
        return res.status(400).send('Invalid Update');

    try {
        const task = await Task.findById(req.params.id);

        updates.forEach((update) => task[update] = req.body[update]);
        await task.save();

        if(!task)
            return res.status(400).send('Invalid Update');

        res.status(200).send(task);
    }
    catch(e) {
        res.status(400).send(e);
    }

    //PROMISE
    // const updates = Object.keys(req.body);
    // const allowedUpdates = ['description', 'completed'];

    // const isValidUpdate = updates.every((update) => allowedUpdates.includes(update));

    // if (!isValidUpdate)
    //     return res.status(400).send('Invalid Update');

    // const updatedTask = Task.findByIdAndUpdate(req.params.id, req.body, {new: true, runValidators: true})
    
    // updatedTask.then((task) => {
    //     if (!task) 
    //         return res.status(404).send('Task NOT found');

    //     res.status(200).send(task);
    // }).catch((error) => {
    //     res.status(400).send(error.message);
    // });
});

router.delete('/tasks/:id', (req, res) => {
    Task.findByIdAndDelete(req.params.id).then((task) => {
        if (!task)
            res.status(404).send('Task NOT found');

        res.status(200).send(task);
    }).catch((error) => {
        res.status(400).send(error.message);
    });
});


module.exports = router;
const express = require('express');
const auth = require('../middlewares/auth');
const User = require('../models/usersModel');
const router =  new express.Router();

/* ---USERS--- */

router.get('/users', auth, async (req, res) => {

    //ASYNC-AWAIT
    try {
        const tasks = await User.find({});
        res.status(200).send(tasks);
    } catch (e) {
        res.status(400).send(e);
    }

    //PROMISE
    // User.find({})
    // .then((tasks) => {
    //     res.send(tasks);
    // }).catch((error) => {
    //      res.status(400).send(error.message);
    // });
});

router.get('/users/me', auth, async (req, res) => {
    res.send(req.user);
});

router.get('/users/:id', auth, async (req, res) => {

    //ASYNC-AWAIT
    try {
        const user = await User.findById(req.params.id);
        if (!user)
            return res.status(404).send('User NOT found');

        res.status(200).send(user);
    } catch (e) {
        res.status(500).send(e);
    }

    //PROMISE
    // User.findById(req.params.id).then((user) => {
    //     if(!user)
    //         return res.status(404).send('User NOT found');

    //     res.status(200).send(user);
    // }).catch((error) => {
    //     res.status(400).send(error.message);
    // });
});

router.post('/users', async (req, res) => {
    const user = new User(req.body);

    //ASYNC-AWAIT
    try {
        await user.save();

        const token = await user.generateAuthToken();
        res.status(201).send({user: user, userToken: token});
    } catch (e) {
        res.status(400).send(e);
    }

    //PROMISE
    // user.save().then(() => {
    //     res.status(201).send(user);
    // }).catch((error) => {
    //     res.status(400).send(error.message);
    // });
});

router.post('/users/login', async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken();

        res.status(200).send({user: user, userToken: token});
    }
    catch(e) {
        res.status(400).send(e.message);
    }
});

router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => {
            return token.token !== req.token;
        });

        await req.user.save();
        res.status(200).send();
    }
    catch(e) {
        res.status(500).send();
    }
});

router.patch('/users/:id', async (req, res) => {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['name', 'email', 'password', 'age'];

    const isValidUpdate = updates.every((update) => allowedUpdates.includes(update));

    if (!isValidUpdate)
        return res.status(400).send('Invalid Update');

    try {
        const user = await User.findById(req.params.id);

        updates.forEach((update) => user[update] = req.body[update]);
        await user.save();
        // const user = await User.findByIdAndUpdate(req.params.id, req.body, {new: true, runValidators: true});

        if (!user)
            return res.status(404).send('User NOT found');

        res.send(user);
    } catch (e) {
        res.status(500).send(e.message);
    }
});

router.delete('/users/:id', async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id);
        if (!user)
            return res.status(404).send('User NOT found');

        res.send(user);
    } catch (e) {
        res.status(500).send(e);
    }
});


module.exports = router;
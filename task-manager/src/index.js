const express = require('express');
const jwt = require('jsonwebtoken');
const userRouter = require('./routers/users');
const taskRouter = require('./routers/tasks');
const auth = require('./middlewares/auth');
require('./db/mongoose');

const app = express();

app.use(express.json());
app.use(userRouter);
app.use(taskRouter);

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Server is up and running on port: ${port}`);
});
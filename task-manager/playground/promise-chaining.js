require('../src/db/mongoose');
const User = require('../src/models/user');

// User.findByIdAndUpdate('5c8902163c6ec320b0b2184f', { age: 1 }).then((user) => {
//     console.log(user)
//     return User.countDocuments({ age: 1 });
// }).then((result) => {
//     console.log(result);
// }).catch((e) => {
//     console.log(e);
// });

const updateUserCount = async (id, age) => {
    const user = await User.findByIdAndUpdate(id, { age: age });
    const count = await User.countDocuments({ age: age });

    // return both using object
    // return { user, count };

    return count;
};

updateUserCount('5c89060c468c717004462be9', 22).then((count) => {
    console.log(count);
}).catch((error) => {
    console.log(error.message);
});
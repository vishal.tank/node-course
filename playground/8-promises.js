// const doWorkPromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         // resolve('Resolved');
//         reject('Rejected');
//     });
// });

// doWorkPromise.then((result) => {
//     console.log(result);
// }).catch((error) => {
//     console.log(error);
// });

const add = (a, b) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(a+b);
        }, 3000);
    });
};

// add(1, 2).then((sum) => {
//     console.log(sum);

//     add(sum, 3).then((sum1) => {
//         console.log(sum1);

//         add(sum1, 4).then((sum2) => {
//             console.log(sum2);
//         }).catch((error) => {
//             console.log(error);   
//         });

//     }).catch((error) => {
//         console.log(error);
//     });

// }).catch((error) => {
//     console.log(error);
// });


add(1, 2).then((sum) => {
    console.log(sum);

    return add(sum, 3);
}).then((sum1) => {
    console.log(sum1);

    return add(sum1, 4);
}).then((sum2) => {
    console.log(sum2);
});
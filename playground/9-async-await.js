const add = (a, b) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(a+b);
        }, 3000);
    });
};

const doWork = async () => {
    const sum = await add(1, 2);
    const sum1 = await add(sum, 3);
    const sum2 = await add(sum1, 4);
    return sum2;
};

doWork().then((res) => {
    console.log(res);
}).catch((error) => {
    console.log(error.message);
})